﻿using HealthMeApp2.Client;
using HealthMeApp2.Models;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HealthMeApp2.UI
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            Title = "Login Page";
        }     

        private async void OnButtonClicked(object sender, EventArgs args)
        {            
            var client = new ValidLogin();
            var logIn = client.Users.LoginUser(usr.Text, psw.Text);
            if (logIn == true)
            {
                var aa = new ValidLogin();
                var id  = aa.Users.GetIdUser(usr.Text, psw.Text);
                Util.UserId = id ?? 0;
                                             
               GetCurrentLocation();
               await Navigation.PushModalAsync(new QuestionnaireListView());
            }
            else await Navigation.PushModalAsync(new LoginPage());
        }

        async void OnRegisterClicked(object sender, EventArgs args)
        {
            await Navigation.PushModalAsync(new RegisterPage());
        }

        //Get location
        public async void  GetCurrentLocation()
        {
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;
            var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);

            if (position == null)
            {
                await DisplayAlert("Ooops!", "Location could not be acquired, geolocator is disabled.", "Ok");               
            }
            var location = new ValidLogin();
            location.Users.PostLong(Util.UserId, position.Longitude, position.Latitude);
        }








        //Get location
        //public static async Task<Position> GetCurrentLocation()
        //{
        //    // Capture the current location.
        //    Position position = null;
        //    try
        //    {
        //        var locator = CrossGeolocator.Current;
        //        locator.DesiredAccuracy = 50;

        //        // Get the current device position. Leave it null if geo-location is disabled,
        //        // return position (0, 0) if unable to acquire.
        //        if (locator.IsGeolocationEnabled)
        //        {
        //            // Allow ten seconds for geo-location determination.                    
        //            position = await locator.GetPositionAsync(10000);
        //            position.Latitude = lat;
        //            position.Longitude = lon;
        //        }

            //    else
            //    {
            //        Debug.WriteLine("Location could not be acquired, geolocator is disabled.");
            //    }
            //}
            //catch (Exception le)
            //{
            //    // TODO: Log this error.
            //    Debug.WriteLine("Location could not be acquired.");
            //    Debug.WriteLine(le.Message);
            //    Debug.WriteLine(le.StackTrace);
            //    position = new Position() { Latitude = 0, Longitude = 0 };
            //}

            //return position;
            //{
            //    latitude = lat,
            //    longitude = lon


            //};
   // }

        //async void OnFBbuttonClicked(object sender, EventArgs args)
        //{
        //    await Navigation.PushModalAsync(new QuestionnaireListView());
        //}
    }
}

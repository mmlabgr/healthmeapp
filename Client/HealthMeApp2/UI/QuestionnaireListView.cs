﻿using HealthMeApp2.Client;
using HealthMeApp2.Client.Models;
using HealthMeApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace HealthMeApp2.UI
{
    public class QuestionnaireListView : ContentPage
    {        
        static StackLayout layout = new StackLayout()
        {
            Orientation = StackOrientation.Vertical,
            VerticalOptions = LayoutOptions.FillAndExpand,
            BackgroundColor = Color.FromHex("#fae3e3"),
            Children =
            {  new Label {
                           Text ="Q u e s t i o n n a i r e",
                           VerticalOptions = LayoutOptions.CenterAndExpand,
                           HorizontalOptions = LayoutOptions.CenterAndExpand,
                           TextColor =Color.FromHex("#c40091"),                         
                           FontSize=25,
                           FontFamily="Trebuchet MS",
                           FontAttributes = FontAttributes.Bold         
                        }
            }
        };

        ScrollView scrollView = new ScrollView()
        {
            HorizontalOptions = LayoutOptions.Fill,
            Orientation = ScrollOrientation.Vertical,
            Content =  layout
        };

        IList<IdValue> questions = DataSourceQuestion(); //epistrefei to ID kai to NAME tis kathe erotisis

        public QuestionnaireListView()
        {           
            //ListView listview = new ListView();
            //var itemTemplate = new DataTemplate(typeof(Label));          
            foreach (var item in questions) //Κάθε αντικείμενο στη λίστα Ερωτήσεων...
            {
                layout.Children.Add(new Label()
                {
                    HorizontalOptions = LayoutOptions.Start,
                    Text = item.Name,
                    TextColor = Color.FromHex("#c40091"),
                    FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                    FontAttributes = FontAttributes.Bold,
                    //BackgroundColor=Color.Black
                });

                int num = DataSourceNumAnswers(item.Id ?? 0); //Eπιστρέφει Αριθμό απαντήσεων
                IList<IdNameAnswer> answers = DataSourceAnswer(item.Id ?? 0); //Λίστα με τις απαντήσεις(Name,Id). Όρισμα: Id ερώτησης
                IEnumerable<string> anwr_name = answers.Select(n => n.Name); //Το ss περιέχει μόνο τα ονόματα των Απαντήσεων που έχουν επιστραφεί παραπάνω
                IEnumerable<int> anwr_id = answers.Select(i => i.Id ?? 0);

                CheckBoxWithTag[] chk = new CheckBoxWithTag[num]; //Δημιουργώ έναν πίνακα που περιέχει num αριθμό Checkboxes (Το num έχει επιστραφεί παραπάνω)

                for (int i = 0; i <= num - 1; i++) //Για κάθε ένα Checkbox...
                {
                    chk[i] = new CheckBoxWithTag();
                    chk[i].DefaultText = anwr_name.ElementAt(i); //Θέτει στο UncheckedText το όνομα της απάντησης 
                    chk[i].Tag = anwr_id.ElementAt(i);
                    chk[i].TextColor = Color.FromHex("#770058");
                    chk[i].FontSize = 16;
                    // chk[i] += chk_Click;
                    layout.Children.Add(chk[i]);
                }
              
            }
            //------Button Submit -----------------//
            Button button = new Button();
            button.Text = "Submit";
            button.TextColor = Color.FromHex("#fae3e3");
            button.BackgroundColor = Color.FromHex("#c40091");
            button.Font = Font.Default;
            button.BorderWidth = 14;
            button.BorderRadius = 5;
            button.Clicked += OnButtonClicked;
            layout.Children.Add(button);

            //------Button Show My Tips -----------------//
            Button buttonTip = new Button();
            buttonTip.Text = "Show the Tips";
            buttonTip.TextColor = Color.FromHex("#fae3e3");
            button.BackgroundColor = Color.FromHex("#c40091");
            buttonTip.Font = Font.Default;
            buttonTip.BorderWidth = 14;
            buttonTip.BorderRadius = 5;
            buttonTip.Clicked += OnButtonTipClicked;
            layout.Children.Add(buttonTip);
            
            this.Content = scrollView;

        }
        //METHODS
        //____1____ Φέρνω τις Ερωτήσεις (Name και Id)
        public static IList<IdValue> DataSourceQuestion()
        {
            var client = new ValidLogin();
            var Questions = client.Question.ReturnQuestionNameId(); //epistrefei antikeimeno typo IdValue
            return Questions;
        }

        //____2____ Aριθμό απαντήσεων για κάθε ερώτηση. Όρισμα: i->id Eρώτησης
        public int DataSourceNumAnswers(int i)
        {
            var client = new ValidLogin();
            var numAnswers = client.Question.NumberOfAnwersByQuestionID(i);
            return numAnswers ?? 0;
            
        }

        //____3____ Φέρνω τις Απαντήσεις για κάθε ερώτηση (Name και Id). Όρισμα: id_q->id της Ερώτησης
        public IList<IdNameAnswer> DataSourceAnswer(int id_q)
        {
            var client = new ValidLogin();
            var Answers = client.Answer.AnswersByQuestionId(id_q);
            return Answers;
        }

        //___4____ Στο πάτημα του Submit εκτελείται. Ελέγχει κάθε checked checkbox
        void OnButtonClicked(object sender, EventArgs e)
        {
            foreach (var item_an in layout.Children)
            {
                if (item_an is CheckBoxWithTag)
                {
                    if (((CheckBoxWithTag)item_an).Checked)
                    {
                        ((CheckBoxWithTag)item_an).CheckedChanged += chk_Click;
                        var ch_tag = ((CheckBoxWithTag)item_an).Tag;
                        var client = new ValidLogin();
                        client.Answer.PostAnswers(ch_tag, Util.UserId);
                    }
                }
            }
           var client1 = new ValidLogin();
           client1.Tip.InsertTipsOfIdUser(Util.UserId);
           Navigation.PushModalAsync(new UserTip(Util.UserId));
        }

        //___5____ Στο πάτημα του Show my tips εκτελείται. Connect me WebDrools
         private async void OnButtonTipClicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new UserTip(Util.UserId));
        }

        //___6____ Eνεργοποίηση ενός checkbox σε κάθε ερώτηση
        CheckBoxWithTag lastChecked;
        private void chk_Click(object sender, EventArgs e)
        {
            CheckBoxWithTag activeCheckBox = sender as CheckBoxWithTag;
            if (activeCheckBox != lastChecked && lastChecked != null) lastChecked.Checked = false;
            lastChecked = activeCheckBox.Checked ? activeCheckBox : null;
        }
    }
}

﻿
using HealthMeApp2.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace HealthMeApp2.UI
{
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
            Title = "Register Page";
        }

        async void OnAccountClicked(object sender, EventArgs args)
        {
            var client = new ValidLogin();
            var register = client.Users.CheckRegisterUser(r_usr.Text);
            if (register == true)
            {
                await DisplayAlert("Warning!", "This username is already taken. Please try another one.", "OK");
            }
            else
            {
                var new_client= new ValidLogin();
                new_client.Users.PostRegister(r_usr.Text, r_psw.Text);
                await DisplayAlert("Success!", "Registered Successfully", "OK");
                await Navigation.PushModalAsync(new LoginPage());
            }
        }

     }
}

﻿using System.Collections.Generic;
using Xamarin.Forms;
using HealthMeApp2.Client;
using HealthMeApp2.Models;
using System.Collections.ObjectModel;

namespace HealthMeApp2.UI
{
    public partial class UserTip : ContentPage
    {
       public static IList<string> UsrTip { get; set; }        
       public UserTip() { }

        public UserTip(int userid)
        {
            InitializeComponent();
            DataSource();
        }

        public void DataSource()
        {
            var items = new List<TipUI>();
            var clientT = new ValidLogin();
            UsrTip = clientT.Tip.ReturnTipByIdUser(Util.UserId);    
            for (int i=0; i<UsrTip.Count; i++)
             {               
                items.Add(new TipUI() { Name = "-" + UsrTip[i] });
             }
            listView.ItemsSource = items;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XLabs.Forms.Controls;

namespace HealthMeApp2.Models
{
    class CheckBoxWithTag : CheckBox
    {
        public int Tag { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthMeApp2.Models
{
    public class MyPosition
    {
        public float latitude { get; set; }
        public float longitude { get; set; }
    }
}

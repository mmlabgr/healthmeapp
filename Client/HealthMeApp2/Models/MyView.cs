﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HealthMeApp2.Models
{
    class MyView : View
    {
        public event EventHandler<EventArgs> OnCheckedChangedCustom;

        protected void CheckedChanged(EventArgs e)
        {
            if (OnCheckedChangedCustom != null)
            {
                OnCheckedChangedCustom(this, EventArgs.Empty);
            }
            
        }
    }
}


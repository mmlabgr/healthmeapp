﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ValidLogin.Classes
{
    public class ResponseClass
    {
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute("org.kie.server.api.model.ServiceResponse", Namespace = "", IsNullable = false)]
        public partial class orgkieserverapimodelServiceResponse
        {

            private string typeField;

            private string msgField;

            private orgkieserverapimodelServiceResponseResult resultField;

            /// <remarks/>
            public string type
            {
                get
                {
                    return this.typeField;
                }
                set
                {
                    this.typeField = value;
                }
            }

            /// <remarks/>
            public string msg
            {
                get
                {
                    return this.msgField;
                }
                set
                {
                    this.msgField = value;
                }
            }

            /// <remarks/>
            public orgkieserverapimodelServiceResponseResult result
            {
                get
                {
                    return this.resultField;
                }
                set
                {
                    this.resultField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class orgkieserverapimodelServiceResponseResult
        {

            private orgkieserverapimodelServiceResponseResultResult resultField;

            private orgkieserverapimodelServiceResponseResultFacthandle facthandleField;

            private string classField;

            /// <remarks/>
            public orgkieserverapimodelServiceResponseResultResult result
            {
                get
                {
                    return this.resultField;
                }
                set
                {
                    this.resultField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("fact-handle")]
            public orgkieserverapimodelServiceResponseResultFacthandle facthandle
            {
                get
                {
                    return this.facthandleField;
                }
                set
                {
                    this.facthandleField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string @class
            {
                get
                {
                    return this.classField;
                }
                set
                {
                    this.classField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class orgkieserverapimodelServiceResponseResultResult
        {

            private orgkieserverapimodelServiceResponseResultResultDemoallnewrulesUser demoallnewrulesUserField;

            private string identifierField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("demo.allnewrules.User")]
            public orgkieserverapimodelServiceResponseResultResultDemoallnewrulesUser demoallnewrulesUser
            {
                get
                {
                    return this.demoallnewrulesUserField;
                }
                set
                {
                    this.demoallnewrulesUserField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class orgkieserverapimodelServiceResponseResultResultDemoallnewrulesUser
        {

            private string sexField;

            private byte ageField;

            private string slp__hoursField;

            private string alc__drinkingField;

            private string stressField;

            private string ph__activityField;

            private string smokingField;

            private string breakfastField;

            private string wtr__drinkingField;

            private string tipSmokField;

            private string tipDrinkField;

            private string tipWaterField;

            private string tipStressField;

            private string tipPhysicalField;

            private string tipBreakfastField;

            private string tipSleepField;

            private string ageSField;

            /// <remarks/>
            public string sex
            {
                get
                {
                    return this.sexField;
                }
                set
                {
                    this.sexField = value;
                }
            }

            /// <remarks/>
            public byte age
            {
                get
                {
                    return this.ageField;
                }
                set
                {
                    this.ageField = value;
                }
            }

            /// <remarks/>
            public string slp__hours
            {
                get
                {
                    return this.slp__hoursField;
                }
                set
                {
                    this.slp__hoursField = value;
                }
            }

            /// <remarks/>
            public string alc__drinking
            {
                get
                {
                    return this.alc__drinkingField;
                }
                set
                {
                    this.alc__drinkingField = value;
                }
            }

            /// <remarks/>
            public string stress
            {
                get
                {
                    return this.stressField;
                }
                set
                {
                    this.stressField = value;
                }
            }

            /// <remarks/>
            public string ph__activity
            {
                get
                {
                    return this.ph__activityField;
                }
                set
                {
                    this.ph__activityField = value;
                }
            }

            /// <remarks/>
            public string smoking
            {
                get
                {
                    return this.smokingField;
                }
                set
                {
                    this.smokingField = value;
                }
            }

            /// <remarks/>
            public string breakfast
            {
                get
                {
                    return this.breakfastField;
                }
                set
                {
                    this.breakfastField = value;
                }
            }

            /// <remarks/>
            public string wtr__drinking
            {
                get
                {
                    return this.wtr__drinkingField;
                }
                set
                {
                    this.wtr__drinkingField = value;
                }
            }

            /// <remarks/>
            public string tipSmok
            {
                get
                {
                    return this.tipSmokField;
                }
                set
                {
                    this.tipSmokField = value;
                }
            }

            /// <remarks/>
            public string tipDrink
            {
                get
                {
                    return this.tipDrinkField;
                }
                set
                {
                    this.tipDrinkField = value;
                }
            }

            /// <remarks/>
            public string tipWater
            {
                get
                {
                    return this.tipWaterField;
                }
                set
                {
                    this.tipWaterField = value;
                }
            }

            /// <remarks/>
            public string tipStress
            {
                get
                {
                    return this.tipStressField;
                }
                set
                {
                    this.tipStressField = value;
                }
            }

            /// <remarks/>
            public string tipPhysical
            {
                get
                {
                    return this.tipPhysicalField;
                }
                set
                {
                    this.tipPhysicalField = value;
                }
            }

            /// <remarks/>
            public string tipBreakfast
            {
                get
                {
                    return this.tipBreakfastField;
                }
                set
                {
                    this.tipBreakfastField = value;
                }
            }

            /// <remarks/>
            public string tipSleep
            {
                get
                {
                    return this.tipSleepField;
                }
                set
                {
                    this.tipSleepField = value;
                }
            }

            /// <remarks/>
            public string ageS
            {
                get
                {
                    return this.ageSField;
                }
                set
                {
                    this.ageSField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class orgkieserverapimodelServiceResponseResultFacthandle
        {

            private string identifierField;

            private string externalformField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string identifier
            {
                get
                {
                    return this.identifierField;
                }
                set
                {
                    this.identifierField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute("external-form")]
            public string externalform
            {
                get
                {
                    return this.externalformField;
                }
                set
                {
                    this.externalformField = value;
                }
            }
        }





        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //[System.Xml.Serialization.XmlRootAttribute("org.kie.server.api.model.ServiceResponse", Namespace = "", IsNullable = false)]
        //public partial class orgkieserverapimodelServiceResponse
        //{

        //    private string typeField;

        //    private string msgField;

        //    private orgkieserverapimodelServiceResponseResult resultField;

        //    /// <remarks/>
        //    public string type
        //    {
        //        get
        //        {
        //            return this.typeField;
        //        }
        //        set
        //        {
        //            this.typeField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string msg
        //    {
        //        get
        //        {
        //            return this.msgField;
        //        }
        //        set
        //        {
        //            this.msgField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public orgkieserverapimodelServiceResponseResult result
        //    {
        //        get
        //        {
        //            return this.resultField;
        //        }
        //        set
        //        {
        //            this.resultField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class orgkieserverapimodelServiceResponseResult
        //{

        //    private orgkieserverapimodelServiceResponseResultResult resultField;

        //    private orgkieserverapimodelServiceResponseResultFacthandle facthandleField;

        //    private string classField;

        //    /// <remarks/>
        //    public orgkieserverapimodelServiceResponseResultResult result
        //    {
        //        get
        //        {
        //            return this.resultField;
        //        }
        //        set
        //        {
        //            this.resultField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute("fact-handle")]
        //    public orgkieserverapimodelServiceResponseResultFacthandle facthandle
        //    {
        //        get
        //        {
        //            return this.facthandleField;
        //        }
        //        set
        //        {
        //            this.facthandleField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlAttributeAttribute()]
        //    public string @class
        //    {
        //        get
        //        {
        //            return this.classField;
        //        }
        //        set
        //        {
        //            this.classField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class orgkieserverapimodelServiceResponseResultResult
        //{

        //    private orgkieserverapimodelServiceResponseResultResultDemomyrulesUser demomyrulesUserField;

        //    private string identifierField;

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlElementAttribute("demo.myrules.User")]
        //    public orgkieserverapimodelServiceResponseResultResultDemomyrulesUser demomyrulesUser
        //    {
        //        get
        //        {
        //            return this.demomyrulesUserField;
        //        }
        //        set
        //        {
        //            this.demomyrulesUserField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlAttributeAttribute()]
        //    public string identifier
        //    {
        //        get
        //        {
        //            return this.identifierField;
        //        }
        //        set
        //        {
        //            this.identifierField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class orgkieserverapimodelServiceResponseResultResultDemomyrulesUser
        //{

        //    private string sexField;

        //    private byte ageField;

        //    private string tipField;

        //    /// <remarks/>
        //    public string sex
        //    {
        //        get
        //        {
        //            return this.sexField;
        //        }
        //        set
        //        {
        //            this.sexField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public byte age
        //    {
        //        get
        //        {
        //            return this.ageField;
        //        }
        //        set
        //        {
        //            this.ageField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    public string tip
        //    {
        //        get
        //        {
        //            return this.tipField;
        //        }
        //        set
        //        {
        //            this.tipField = value;
        //        }
        //    }
        //}

        ///// <remarks/>
        //[System.SerializableAttribute()]
        //[System.ComponentModel.DesignerCategoryAttribute("code")]
        //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        //public partial class orgkieserverapimodelServiceResponseResultFacthandle
        //{

        //    private string identifierField;

        //    private string externalformField;

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlAttributeAttribute()]
        //    public string identifier
        //    {
        //        get
        //        {
        //            return this.identifierField;
        //        }
        //        set
        //        {
        //            this.identifierField = value;
        //        }
        //    }

        //    /// <remarks/>
        //    [System.Xml.Serialization.XmlAttributeAttribute("external-form")]
        //    public string externalform
        //    {
        //        get
        //        {
        //            return this.externalformField;
        //        }
        //        set
        //        {
        //            this.externalformField = value;
        //        }
        //    }
        //}
    }
}
